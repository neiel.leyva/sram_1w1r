//   ---------------------------------------------------------- 
//   - File           : tb_sram_1r1w.sv                
//   - Project        : sram_1r1w                   
//   - Organization   : Neiel Leyva - Education           
//   - Author         : Neiel Israel Leyva Santes         
//   - Email          : israel.leyva.santes@gmail.com       
//   - MIT License
//  
//   Copyright (c) 2022 Neiel Leyva - Education
//               
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//               
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//                       
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.   
//   ----------------------------------------------------------

`timescale 1 ns/ 1 ns

module tb_sram_1r1w();

localparam SRAM_WIDTH = 128  ; // size of each entry
localparam SRAM_DEPTH = 1024 ; // number of entries
    
logic                           clk    ;      
logic                           req    ;      
logic                           we     ;      
logic  [SRAM_WIDTH-1:0]         wdata  ;      
logic  [$clog2(SRAM_DEPTH)-1:0] addr   ;      
logic  [SRAM_WIDTH-1:0]         rdata  ;  

logic  [$clog2(SRAM_DEPTH)-1:0] cntr   ;      

// Design under test
sram_1w1r #(  
    .SRAM_WIDTH(SRAM_WIDTH),
    .SRAM_DEPTH(SRAM_DEPTH)
) __DUT__ (
    .clk_i ( clk   ),    
    .req_i ( req   ),    
    .we_i  ( we    ),    
    .data_i( wdata ),    
    .addr_i( addr  ),    
    .data_o( rdata )     
);

// main
initial begin
    initialization(); 
    
    @(posedge clk);
    repeat(SRAM_DEPTH-1) write_process(); 
    
    @(posedge clk) req  = 1'b0 ; 
                   cntr = 1    ; 
                   we   = 1'b0 ;

    @(posedge clk);  
    repeat(SRAM_DEPTH-1) read_process(); 
        
    @(posedge clk) req = 1'b0 ; 
  
    $finish;
end    

// Clock signal
always #25 clk = ~clk;

task automatic initialization;
begin
    clk   =  1'b1 ; 
    req   =  1'b0 ; 
    we    =  1'b0 ; 
    cntr  =  1    ; 
    addr  =  '0   ; 
    wdata =  '0   ;
end
endtask

task automatic write_process; 
begin
    req   =  1'b1   ; 
    we    =  1'b1   ; 
    addr  =  cntr   ; 
    wdata =  cntr   ;
    @(posedge clk)  ;  
    cntr  =  cntr+1 ;
end
endtask

task automatic read_process; 
begin
    req   =  1'b1   ; 
    addr  =  cntr   ; 
    @(posedge clk)  ;
    cntr  =  cntr+1 ;
    #1 monitor()    ; 
end
endtask

task automatic monitor; 
begin    
    $write ("time %0d ns  ", $time);
    if ( addr == rdata ) 
        $display (" <read> addr: 0x%0h data: 0x%0h -> ok", addr,rdata);
    else begin
        $display (" <read> addr: 0x%0h data: 0x%0h -> warning", addr,rdata);
        $error ("The data reading was incorrect.");
        $finish;
    end
end
endtask


endmodule
