![Neiel Leyva Education](/doc/banner_readme.jpg?raw=true)


# Single-Port Synchronous SRAM
Static random-access memory (SRAM) is a type of random-access memory (RAM). It is a volatile memory, that is, the data is lost when the power is cut off. It is typically used for the cache and internal registers of a CPU.


The following code can be used to describe a single-port SRAM behaviour. A single port means that it only accepts one request per cycle, read or write, but not both.

#### Behaviour

The following diagram describes the behavior of this single port SRAM with ```n``` elements. The number of elements is specified by ```SRAM_DEPTH``` and the size of each element by ```SRAM_WIDTH```.

- ```req_i``` specifies a valid request. It can be a read or write request.
- ```we_i```  specifies the type of request. ```1``` for a write request and ```0``` for a read request.
- ```addr_i``` is the address to read or write.
- ```data_i``` is the value to write in ```addr_i```. This new value will be available in the next cycle.
- ```data_o``` is the value read at ```addr_i```. It is delivered in the next cycle after the request.


![SRAM behaviour](/doc/sram_beha.png?raw=true)


#### Running testbench

In the directory ```/tb```, you can find a testbench ```tb_sram_1w1r.sv``` and a script to run the testbench ```run_questasim.sh```. The script is compatible with Modelsim/Questasim. It is necessary to have the environment variables related to ```vsim``` configured.

To run the testbench with a graphical interface:
```
$ cd tb
$ ./run_questasim.sh
```

To run the testbench without a graphical interface, batch mode:
```
$ cd tb
$ ./run_questasim.sh -batch
```

#### License

All source code are released under the MIT license. See LICENSE for details.
