//   ---------------------------------------------------------- 
//   - File           : sram_1r1w.sv                
//   - Project        : sram_1r1w                   
//   - Organization   : Neiel Leyva - Education           
//   - Author         : Neiel Israel Leyva Santes         
//   - Email          : israel.leyva.santes@gmail.com       
//   - MIT License
//  
//   Copyright (c) 2022 Neiel Leyva - Education
//               
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//               
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//                       
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.   
//   ----------------------------------------------------------

module sram_1w1r #(
    parameter SRAM_WIDTH = 32 , // size of each entry
    parameter SRAM_DEPTH = 64   // number of entries
)(   
    input  logic                           clk_i  , // clock
    input  logic                           req_i  , // valid request
    input  logic                           we_i   , // write enable
    input  logic  [SRAM_WIDTH-1:0]         data_i , // data to write
    input  logic  [$clog2(SRAM_DEPTH)-1:0] addr_i , // r/w address
    output logic  [SRAM_WIDTH-1:0]         data_o   // data read
);
    
logic [SRAM_WIDTH-1:0] sram [0:SRAM_DEPTH-1];

always_ff @(posedge clk_i) begin
    if(req_i) begin
        if(we_i) sram[addr_i] <= data_i;
        else data_o <= sram[addr_i];
    end
end

`ifdef DEBUG 
    always_ff @(posedge clk_i) begin
        if(req_i & (addr_i >= SRAM_DEPTH)) begin
            $write ("time %0d ns  ", $time);
            if(we_i) $display (" <write> addr: 0x%0h",addr_i);
            else $display (" <read> addr: 0x%0h",addr_i);
            $error ("The address is out of range.");
        end
    end
`endif



endmodule
